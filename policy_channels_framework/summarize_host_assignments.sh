#!/bin/bash

assignments_dir="/var/cfengine/host_channel_assignments"

usage() {
  cat << XXX
usage: $0 -s
   or: $0 -a
   or: $0 <channel_name>..
   or: $0 -h

    -s, --summary        Show summary of how many hosts assigned to each channel
    -a, --all            Show all channels and the hosts assigned to them
    -h, --help           Show this usage message

NOTE: This script only reports on the assignment files on the policy server.
      For up to date info on exactly which policy set a given host is running,
      log in to the host.
XXX
}

print_channel_data() {
  termwidth="$(tput cols)"
  padding="$(printf '%0.1s' ={1..500})"

  for channel in "$@"; do
    # Print header, centered in middle of terminal and padded with '='
    printf '%*.*s %s %*.*s\n' 0 "$(((termwidth-2-${#channel})/2))" "$padding" "$channel" 0 "$(((termwidth-1-${#channel})/2))" "$padding"
    # Print hosts on that channel
    grep -lxF "$channel" ${assignments_dir}/*.txt | sed -e 's:^.*/::;s:\.txt$::'
  done | more
}

[ "$#" -eq 0 ] && { usage; exit 0;}

case "$1" in
  -h|--help)
    usage
    exit 0
    ;;
  -s|--summary)
    awk ' {
            channels[$0]++
          }
          END {
            for (channel in channels) {
              print channels[channel] " hosts are assigned to " channel
            }
          }' ${assignments_dir}/*.txt
    exit "$?"
    ;;
  -a|--all)
    print_channel_data $(sed -nep ${assignments_dir}/*.txt | sort | uniq)
    exit 0
    ;;
  -*|--*)
    printf %s\\n "Unrecognized option: $1"
    usage
    exit 1
    ;;
  *)
    print_channel_data "$@"
    exit 0
    ;;
esac
