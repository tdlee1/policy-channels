#!/bin/bash

# This script accepts as an argument a text file containing a list of hostnames.
# It assigns each host to the policy channel that has the same name as the text file,
# minus the .txt extension.

# If there is no policy channel with that name (i.e. there is no dir in
# /var/cfengine/policy_channels/ with that name)
# or if the policy channel dir doesn't contain both update.cf and promises.cf ,
# this script will refuse to assign hosts to that channel.

# If any of the hosts are already assigned to a different channel, this script will
# silently overwrite that channel assignment!  So don't put hosts in that you don't want
# to assign to the channel!  (No safety net on reassigning hosts - only a safety net
# for assigning hosts to channels that aren't valid channels.)

# The way hosts are assigned to channels is by putting a file named <hostname>.txt
# into /var/cfengine/host_channel_assignments/
# which contains only the name of the policy channel that host is assigned to.

policy_channels_dir="/var/cfengine/policy_channels"
assignments_dir="/var/cfengine/host_channel_assignments"

if [ "$#" -eq 0 ] || [ "--help" = "$1" ] ; then
  cat << EOF
Usage: $0 channelname.txt [anotherchannel.txt ..]

This script simplifies the process of assigning hosts to policy channels.

'channelname.txt' should contain a list of fully qualified hostnames,
one per line, that should be assigned to the channel 'channelname'.
Multiple files containing lists of hosts may be passed and each host listed
in a file will be assigned to the channel with the same name as that file.

Previous channel assignments of those hosts will be silently overwritten.

'channelname.txt' may be given as an absolute path, e.g. /tmp/channelname.txt
and the channelname will be correctly extracted.

'channelname' must be a valid channel existing in
$policy_channels_dir
or $(basename "$0") will complain and exit.
EOF
  exit 0
fi

die () {
  printf '%s\n' "$@"
  exit 1
}

[ -d "$policy_channels_dir" ] ||
  die "Error: $policy_channels_dir not found." "Policy channels must be created in that dir."

mkdir -p "$assignments_dir" ||
  die "Error: $assignments_dir doesn't exist and can't be created."

[ -w "$assignments_dir" ] ||
  die "Error: $assignments_dir is not writable"

# Validate channels
for hostlist in "$@" ; do

  [ -f "$hostlist" ] && [ -r "$hostlist" ] ||
    die "$hostlist not a valid readable file." "No host channel assignments have been changed."

  channelname="$(basename "$hostlist" .txt)"
  channelpath="${policy_channels_dir}/${channelname}"

  if ! { [ -d "${channelpath}" ] && [ -r "${channelpath}/promises.cf" ] && [ -r "${channelpath}/update.cf" ] ;} ; then
    die "$channelname is not a valid channel." "No host channel assignments have been changed."
  fi
done

# Validate hostnames
awk 'NF != 1 {
       print "Error: Illegal whitespace:"
       print "\t" $0
       print "\tFile: " FILENAME
       print "\tLine: " FNR
       exit 1
     }

     $1 ~ /[^A-Za-z0-9._-]/ {
       print "Error: Illegal characters in hostname:"
       print "\t" $1
       print "\tFile: " FILENAME
       print "\tLine: " FNR
       exit 1
     }

     $1 in hostnames {
       print "Error: Hostname appears twice:"
       print "\t" $1
       print hostnames[$1]
       print "\tFile: " FILENAME
       print "\tLine: " FNR
       exit 1
     }

     {
       hostnames[$1] = "\tFile: " FILENAME "\n\tLine: " FNR
     }' "$@" || die "No host channel assignments have been changed."

# Assign hosts
for hostlist in "$@" ; do
  channelname="$(basename "$hostlist" .txt)"
  # The true magic.  All the rest of this script is safety checks; the whole ACTION happens here.
  awk -v channelname="$channelname" -v assignments_dir="${assignments_dir}/" '{printf "%s", channelname > assignments_dir $1".txt" ; close( assignments_dir $1".txt" ) ; count++ } END {print count+0 " hosts assigned to channel: \"" channelname "\"" }' "$hostlist"
done
