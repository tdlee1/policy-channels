Author: Aleksey Tsalolikhin and Mike Weilgart

Date: June 20, 2017

Updated: 21 Sep 2017

See also: [Policy Channels Documentation](/README.md)

# Retiring a policy channel

Question: How do we retire/remove policy channels?

Answer:

- Reassign any hosts still assigned to the channel onto a different channel.
  (Use `summarize_host_assignments.sh` and `rollout_host_assignments.sh`.)
- Verify in Postgres that no host is reporting that channel as its "Masterfiles Source".

  (Note: Postgres refers to the inventory database that is part of CFEngine
  Enterprise.)

```sql
SELECT COUNT(*),
       variablevalue AS "Masterfiles Source"
FROM   variables
WHERE  comp = 'default.select_masterfiles.master_location'
GROUP  BY variablevalue
ORDER  BY count DESC;
```
- Remove the channel definition from `params.sh`
- Remove the policy channel directory itself from disk (under `/var/cfengine/policy_channels/`)
