# Policy Channels Framework Documentation

Author: Mike Weilgart  
Date: 20 January 2016  
Last Updated: 3 March 2016  

Note: Throughout this document, I've assumed that you are generally
familiar with CFEngine's purpose and structure.

Also, this document was written assuming CFEngine Enterprise;
mentions of "the dashboard" are referring to the Enterprise UI
and are not applicable to CFEngine Core.  The rest of the document
still applies.

## History and Background

### Out of the Box

In a typical CFEngine installation, out of the box with little
customization, there is a directory `/var/cfengine/masterfiles` on the
CFEngine policy server where CFEngine policy is placed for distribution
to all the hosts bootstrapped to that policy server.  The hosts pull all
the files from this remote directory and put them in `/var/cfengine/inputs`
locally, which is where they are run from.

The directory `/var/cfengine/masterfiles` is additionally noteworthy because
it is hardcoded in the CFEngine source code as the policy source to use
when first bootstrapping a host to the policy server.  Although it is
possible to customize the CFEngine update policy so that a host copies
its policy set from a different location, a newly bootstrapped host will
nevertheless use the hardcoded location `/var/cfengine/masterfiles` to
get its policy (including the update policy itself!) for the first time.

The workflow out of the box is simple: Put your CFEngine policy set in
`/var/cfengine/masterfiles` on the policy server, and let the bootstrapped
hosts pick it up and run it.

An additional capability that comes with CFEngine out of the box, but
which is not enabled by default, is the option to use some form of Version
Control System to keep track of your policy, and automatically stage that
policy from your version control system into `/var/cfengine/masterfiles`.
(The best VCS integration--and the only one supported out of the box--is
git, but the framework is in place to allow other version control systems
to be integrated.)

When this capability is enabled, a CFEngine policy runs the script
`/var/cfengine/httpd/htdocs/api/dc-scripts/masterfiles-stage.sh` on the
policy server only, to stage the masterfiles from whatever VCS is in
use to `/var/cfengine/masterfiles`.  The masterfiles-stage.sh script that
ships with CFEngine accepts no arguments; the VCS to use is specified in a
"parameters" file, along with several other details needed for staging a
policy set.  (Note: The "dc" in "dc-scripts" stands for "Design Center",
a CFEngine feature which is not commonly used and which I won't discuss
here.)

The adjustable parameters that the masterfiles-stage.sh
script uses are all set in a single file, which is found at
`/opt/cfengine/dc-scripts/params.sh`.  (The path is specified within
masterfiles-stage.sh.)  The parameters include `VCS_TYPE` (which out of
the box can only be set to `GIT`), the URL of the git repository to get
the policy set from (`GIT_URL`), the name of the branch to checkout from
that repository and stage in `/var/cfengine/masterfiles` (`GIT_BRANCH`),
and others.

### Why One Policy Source May Not Be Enough

For small installations of CFEngine with only a few hosts bootstrapped to
a policy server (or even several dozen), the workflow described above is
thoroughly adequate.  The CFEngine policy language itself is quite enough
to specify any differences in configuration required between hosts; it
has many different ways to specify actions conditionally, and a single
policy set can be written which will correctly configure many different
types of hosts.  In fact, a single policy set can correctly configure
*hundreds* or even *thousands* of different types of hosts, each with
their own distinct configuration.  Performance is not the issue here,
nor is policy complexity.

The need for multiple policy channels was initially described as a "phased
rollout" process, and that is still the best description of the problem
that the policy channels framework was conceived to solve.  When changes
are made to a CFEngine policy set that is being run on thousands upon
thousands of customer-facing, production servers, changing all the hosts
to the new policy set at the same time is an invitation for catastrophe.
What was needed was a way to "roll out" a policy set to a small number of
hosts, then a few more, and a few more, checking at each step to ensure
that all was well before continuing.

### Multi Masterfiles Framework

Nick Anderson from CFEngine created the first phased rollout process
with his "Multi Masterfiles Framework" (not published publicly).

The multi masterfiles framework uses two directories as policy sources
instead of just one.  There is `/var/cfengine/masterfiles`, as used in
CFEngine out of the box, and there is also `/var/cfengine/old_masterfiles`.

Nick modified the CFEngine update policy so that each bootstrapped
host would, as part of its update procedure, pull down a copy of
`/var/cfengine/new_masterfiles_hosts.txt` from the policy server.  Each host
would then search that file to see if its own hostname was listed.
If it was found, the host would copy the entire CFEngine policy set
found at `/var/cfengine/masterfiles` (on the policy server) into its inputs
directory and then run it.  If on the other hand a host did *not* find
its hostname listed in `/var/cfengine/new_masterfiles_hosts.txt`, then it
would copy the policy set from `/var/cfengine/old_masterfiles` and use that.

To allow policy to be staged automatically from a git repository
into both the old and new masterfiles directories, Nick expanded the
masterfiles-stage.sh script described earlier.  With his modifications,
the script took two arguments: `MASTERDIR` and `PARAMS`.  These were,
respectively, the path to which the policy set should be staged, and
the path to the "parameters" file to use.

(In actual fact the `PARAMS` file could have contained the `MASTERDIR`
parameter directly, which would have allowed `masterfiles-stage.sh` to be
run with only the single argument `PARAMS`--but this wasn't implemented
until after the policy channels framework was written.)

As before, the `PARAMS` file specified the `VCS_TYPE`, as well as the git
repository to use, the branch to check out from the repository, etc.
Support for checking out a git tag instead of a git branch was added.
(Support for the Subversion version control system was also added.)

To illustrate Nick's changes, the effect of the out of the box
`masterfiles-stage.sh` script could be duplicated by running:

    masterfiles-stage.sh /var/cfengine/masterfiles /opt/cfengine/dc-scripts/params.sh

In summary, a separate `PARAMS` file was created for each target directory,
and the `masterfiles-stage.sh` script was run multiple times: once for each
`PARAMS` file.  The modified script was designed to have broader usability
than just for the multi masterfiles framework; interested CFEngine
customers could have as many separate masterfiles directories as they
wanted, simply by creating a `PARAMS` file for each and running the script
once per `PARAMS` file with the appropriate `MASTERDIR` argument for each.
(Of course, they would have to write their own CFEngine update policy
if they wanted bootstrapped hosts to actually *use* those masterfiles
directories.)

For the multi masterfiles framework, Nick also modified the CFEngine
policy for the policy server itself so that the script would be
run twice: once for `/var/cfengine/old_masterfiles` and once for
`/var/cfengine/masterfiles`.  The exact commands to be run looked like this:

    /opt/cfengine/MYSITE/masterfiles-stage.sh /var/cfengine/masterfiles /opt/cfengine/MYSITE/params_NEW.sh
    /opt/cfengine/MYSITE/masterfiles-stage.sh /var/cfengine/old_masterfiles /opt/cfengine/MYSITE/params_OLD.sh

Therefore, the sysadmin who was to perform a "phased roll out" of a
newly changed CFEngine policy set would do it with the following steps:

1. Ensure params_NEW and params_OLD specified the same git tag, for
   example "old_policy_set".
2. Ensure new_masterfiles_hosts.txt was empty.
3. Change the git tag in params_NEW to be "new_policy_set".
4. Add a few hostnames to new_masterfiles_hosts.txt.
5. Test that everything was working.
6. Repeat steps 4 and 5 until all bootstrapped hostnames had been added
   to the list.
7. Update params_OLD to point to "new_policy_set" and then clear out
   the new_masterfiles_hosts file to be ready for the next release.

As of today, this is the procedure that is still in use.  (Slightly
simplified here for readability.)

### Why Policy Channels Were Still Needed

The multi masterfiles framework had (has) a few specific drawbacks that
prompted the policy channels framework to be created.

First, it only allowed two masterfiles directories for hosts to choose
between.  If a policy set were rolled out to 3000 out of 5000 hosts
(i.e. there were 3000 hostnames listed in new_masterfiles_hosts.txt and
5000 hosts bootstrapped to the policy server) and then a bug in the policy
set were discovered, those 3000 hosts would have to be rolled *back*
to the old policy set so that the hotfix could be rolled out.  There was
no way to have a third version of a policy set "in the pipeline."

Second, the new_masterfiles_hosts.txt file didn't scale well; having
5000 hosts each pulling a copy of a 5000-line file every five or ten
minutes adds up to a lot of unnecessary network traffic.

Third, the new_masterfiles_hosts file had to be recreated for each
separate policy rollout.

Also there was an interesting situation caused by bootstrapping a host
to the policy server without adding it to the new_masterfiles_hosts.txt
file.  Because of the hardcoded path `/var/cfengine/masterfiles` used
when bootstrapping, the host would first get a copy of the CFEngine
policy set from `/var/cfengine/masterfiles`.  (This would include
copying the multi masterfiles framework CFEngine update policy.)
Then, when running its update policy, the host would pull down a copy
of new_masterfiles_hosts.txt.  Not being listed in the file, the host
would then *switch* to old_masterfiles and download and run the policy
set from there.

## Policy Channels Framework

I created the Policy Channels Framework as an improvement upon the multi
masterfiles framework.

### Host Channel Selection

In the policy channels framework, each host attempts to pull a copy
of its channel assignment from the policy server.  Each assignment
file is named based on hostname, and the assignment files are found in
`/var/cfengine/host_channel_assignments/.`  For example, one such file
might be `/var/cfengine/host_channel_assignments/myhostname.fqdn.net.txt`.

There is a separate channel assignment file for every host, rather than
a lookup table, to reduce network traffic--each host only pulls its *own*
assignment file, less than 64 bytes.

If the host has an assignment file, it uses the contents of the file
to determine where to get its policy from.  For example, if the file
contains the text "channel_1", then the host will pull its policy from
`/var/cfengine/policy_channels/channel_1`.

If the host does not have an assignment file (can't download one
from the policy server and doesn't have one locally that was earlier
downloaded from the policy server), then it will pull its policy from
`/var/cfengine/masterfiles`.

This means that `/var/cfengine/masterfiles` is the "default channel", so
to speak.  However the intention is that all hosts that are bootstrapped
will be assigned to policy channels, not left on this "default" channel.
If a host is left without a channel assignment file, it will continue
to run normally and will get any policy updates that are staged in
`/var/cfengine/masterfiles`--but the promise to pull its assignment file
will fail during the update process, which is noisy and will show up
in the Enterprise CFEngine dashboard.

### Staging Policy Channels

The masterfiles-stage.sh script was fully rewritten and
greatly streamlined to allow policy channels to be implemented.
"GIT_POLICY_CHANNELS" was added as a legal value for VCS_TYPE, and rather
than using a separate PARAMS file for each distinct policy channel and
running masterfiles-stage.sh multiple times (as in the multi masterfiles
framework), all the parameters for all policy channels are specified
cleanly in a single PARAMS file.

Additionally, masterfiles-stage.sh was given option parsing
(by Nick Anderson, refined by Mike Weilgart), so that by
default it uses the original location for the PARAMS file
(`/opt/cfengine/dc-scripts/params.sh`), but a different path can still be
specified on the command line if that is desired.

The CFEngine code that runs the script on the
policy server is therefore very short; it just calls
`/var/cfengine/channel_scripts/masterfiles-stage.sh`

When called, the script loads the PARAMS file from the default location,
sees the VCS_TYPE specified as GIT_POLICY_CHANNELS, and loops through
each channel definition to stage the appropriate policy set from the
git repository.

The channels used in the policy channels framework are all placed in
`/var/cfengine/policy_channels`, but any path can be specified in the
PARAMS file for a channel.  (Only those in `/var/cfengine/policy_channels/`
or the default "channel" `/var/cfengine/masterfiles` will be picked up by
hosts, though.)

### Assigning Hosts to Channels

Assigning a host to a channel is as simple as:

    echo channel_name > /var/cfengine/host_channel_assignments/some_host.example.com.txt

However, to make it easy to assign several hosts at once to the
same channel, and to safeguard against hosts being accidentally
assigned to non-existent channels (by typos), I wrote a shell script
"rollout_host_assignments.sh" which will safely assign a list of hosts
to a channel.

For instance:

    $ cat /tmp/channel_1.txt
    host001.fqdn.net
    host002.fqdn.net
    $ cat /tmp/channel_2.txt
    host003.fqdn.net
    host004.fqdn.net
    host005.fqdn.net
    host006.fqdn.net
    host007.fqdn.net
    $ sudo /var/cfengine/channel_scripts/rollout_host_assignments.sh /tmp/channel_1.txt /tmp/channel_2.txt
    2 hosts assigned to channel_1
    5 hosts assigned to channel_2

The rollout_host_assignments.sh script will refuse to assign
hosts to a non-existent channel.  That is, if the directory
`/var/cfengine/policy_channels/channel_1` doesn't exist, or if either
update.cf or promises.cf is missing, the script will complain that it
is an invalid channel and it won't assign any hosts to channel_1.

### Adding or Modifying Channels

To add or modify a channel, modify the PARAMS file found at
`/opt/cfengine/dc-scripts/params.sh`.

Here is a partial example contents of the params file:

    VCS_TYPE="GIT_POLICY_CHANNELS"
    GIT_URL="git@mygitserver.net:joeblow/my_policy_repo.git"
    chan_deploy="/var/cfengine/policy_channels"
    # chan_deploy is not used outside this file
    # and is just for convenience in defining the channel_config array.

    channel_config=()
    channel_config+=( "$chan_deploy/channel_1"    "my_branch_name" )
    channel_config+=( "$chan_deploy/channel_2"    "my_tag_name" )
    channel_config+=( "/var/cfengine/masterfiles" "v_1.0.0" )
    # channel_config+=( "/absolute/path/to/deploy/to"  "git_reference_specifier" )

If you want the channels to be created based on this file without waiting
for the next run of CFEngine on the policy server, you can run:

    cf-agent -f update.cf

or just run the masterfiles-stage.sh script directly:

    /var/cfengine/channel_scripts/masterfiles-stage.sh

(You can add "-D" after the masterfiles-stage.sh script to put
it in "debug" mode and see whether each channel is staged successfully.)

### Phased Rollout Using Policy Channels

To use policy channels for phased rollout, you first must have your
policy channels set up the way you want them.  For example you could
have numbered channels for BAU (Business As Usual) like so: BAU_Phase_1,
BAU_Phase_2, etc.  Create these in the params.sh file as described in
the section "Adding or Modifying Channels".

Once the channels are created, the next step is to assign hosts to them.
Create a list of fully qualified hostnames for each channel, and run
rollout_host_assignments.sh on them as described in the section "Assigning
Hosts to Channels".  This should be a one-time action, though of course
as new hosts are bootstrapped you will need to assign them to channels.

Then, to roll a new version of a policy set out to all hosts:

0. All channels defined in PARAMS should be on "old_policy_set" (or
   "v1.0.7", etc.)
1. Change the reference specifier (refspec) for "BAU_Phase_1" to
   "new_policy_set" (or "v1.0.8", etc.)
2. Test that everything is working.
3. Repeat steps 1 and 2 until all channels have been rolled forward.

As another example, if partway through a rollout you discover a minor
bug and create a new version, you can begin from step 1 and change
"BAU_Phase_1" (etc.) to point to "v1.0.9" without needing to roll any
channels back to 1.0.7 first.  There is a great deal of flexibility.

You can also create a testing channel with only one or two hosts assigned
to it as a sandbox, using a git branch name instead of a git tag so
those hosts always have the latest code from that branch.

You could also create a channel with code on it to handle a one-time
narrow-scope configuration fix, and assign just the hosts that need
that fix to that channel, without impacting all the infrastructure of
all 5000 hosts bootstrapped to that policy server.

In closing, the policy channels framework was designed to give the
sysadmins managing CFEngine a great deal more flexibility with regard
to how they run CFEngine policy, and I hope that it will make your work
easier as well as greatly expand the possible uses to which you can
put CFEngine.

See also: [Fixing hosts that are stuck on old policy (won't pick up new policy)](https://gitlab.com/atsaloli/cfengine-technician/blob/master/fixing_hosts_stuck_on_old_policy.md). Each policy channel has its own `cf_promises_validated` file. Delete that file to force hosts to re-sync from the channel.
