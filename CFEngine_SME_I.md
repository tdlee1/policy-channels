# "CFEngine SME I: Publishing CFEngine Policy" Course

Course Description:

In this course, you will learn policy channels:
- how to create a policy channel,
- how to select what content is published to a policy channel,
- how to subscribe hosts to a channel,
- how to tell what policy channel a host is subscribed to,
- how to tell what policy version a host is running,
- how to move a host to a different channel,
- how to remove a host from a channel,
- how to handle it when a host won't pick up updates,
and
- the releasing process.

## Course Sequence (to be expanded)

### Brief answers to questions about CFEngine lifecycle:

(NOTE: Mentions of "inventory" apply to CFEngine Enterprise only.)

- The CFEngine *software package* itself is not expected to need upgrade
  more often than once a year.
- CFEngine runs *policy* which is pulled down by each individual host
  from the CFEngine Hub.  The policy defines what information should
  be inventoried, and also defines what configuration promises (if any)
  the CFEngine *agent* should apply to that host.
- The inventory and configuration policy (usually just called “CFEngine
  policy”) is stored in version control (Git).
- Any needed changes (e.g. inventory of new information not previously
  collected) are tested first at small scale on test boxes within the
  CFEngine Team.
- The CFEngine Team periodically releases a new version of CFEngine
  policy.
- After rollout to the Lab is complete, the same exact policy version
  tested throughout Lab is turned over to Production.
- An upgrade to the CFEngine software is likewise tested thoroughly
  across the *entire* Lab infrastructure before being handed to Production.
- CFEngine software upgrades are (perhaps surprisingly) even more
  transparent than policy releases, as the agent is running *exactly* the
  same CFEngine policy before and after the software upgrade.  However,
  this depends on the CFEngine policy being upgraded to the latest
  version first.

### Introduction to Policy Channels

[to be filled out]

You should be able to:
-	Juggle hosts on policy channels (to deploy different versions of policy to different hosts, as the need arises, e.g. during a typical rollout of new CFEngine policy).


See also: [Fixing hosts that are stuck on old policy (won't pick up new
policy)](https://gitlab.com/atsaloli/cfengine-technician/blob/master/fixing_hosts_stuck_on_old_policy.md).
Each policy channel has its own `cf_promises_validated` file. Delete
that file to force hosts to re-sync from the channel.
